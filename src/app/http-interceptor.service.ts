import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class HttpInterceptorService implements HttpInterceptor {


  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('INTERCEPTOR');
    // We retrieve the token, if any
    var authToken= '9ee16102-eaf4-11ec-9315-b4b686da73c7';
    let newHeaders = req.headers;
    var timezoneOffset=(new Date()).getTimezoneOffset();
    timezoneOffset=(timezoneOffset)?timezoneOffset:1;

     newHeaders = newHeaders.append('kav-timezoneoffset', ""+timezoneOffset);
    newHeaders = newHeaders.append('auth-token', authToken);
    
    // Finally we have to clone our request with our new headers
    // This is required because HttpRequests are immutable
    const authReq = req.clone({headers: newHeaders});
    // Then we return an Observable that will run the request
    // or pass it to the next interceptor if any
    return next.handle(authReq);
 }
}

