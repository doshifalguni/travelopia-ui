import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const AUTH_API = 'http://localhost:3000/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class HttpserviceService {

  constructor(private http: HttpClient) { }
  postapi(api:string,obj:any): Observable<any> {
    return this.http.post(AUTH_API + api, obj
    , httpOptions);
  }
  getapi(api:any,params:any): Observable<any> {
    return this.http.get(AUTH_API + api, params);
  }
}
