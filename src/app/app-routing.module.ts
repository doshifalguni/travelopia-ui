import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntryPageComponent } from './entry-page/entry-page.component';
import { ListPageComponent } from './list-page/list-page.component';

const routes: Routes = [
  { path: 'entry', component: EntryPageComponent },
  { path: 'list', component: ListPageComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
