import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../notification.service'
import { HttpserviceService } from '../httpservice.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-entry-page',
  templateUrl: './entry-page.component.html',
  styleUrls: ['./entry-page.component.css']
})
export class EntryPageComponent implements OnInit {
  header:any={name:'',emailid:'',place:'',pax:0,budget:0};
  lookups:any={countries:[{id:0,name:'India'},{id:1,name:'Africa'},{id:2,name:'Europe'}]};
  constructor(private notifyService : NotificationService,private httpService:HttpserviceService,private route:Router) { }

  ngOnInit(): void {
    this.header.place=0;
  }

  saveEnq(){
    let _self = this;
    var iserror;
    let mailRegex = /^[a-zA-Z][a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}$/;
    if(!_self.header.name){
      _self.notifyService.showError("Please Enter Name","Error");
      iserror=true;
    }
    if(!_self.header.emailid.match(mailRegex)){
      _self.notifyService.showError("Please Enter Valid Email address","Error");
      iserror=true;
    }
    if(!_self.header.pax){
        _self.notifyService.showError("Please Enter No of Travellers","Error");
        iserror=true;
    }
    if(iserror){
      return;
    }
    _self.httpService.postapi("savedata",_self.header).subscribe({
      next:data=>{
        _self.notifyService.showSuccess((data.message?data.message:"Saved Successfully"),"Success");
        _self.getIntialize();  
        _self.route.navigate(['/list']);

      },error:err=>{
        _self.notifyService.showError(err,"Error")
      }
    })

  }
  getIntialize(){
    let _self=this;
    _self.header.name='';
    _self.header.budget=0;
    _self.header.emailid='';
    _self.header.pax=0;
    _self.header.country=0;
  }
}
