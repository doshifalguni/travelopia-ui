import { Component, OnInit } from '@angular/core';
import { HttpserviceService } from '../httpservice.service';
import { NotificationService } from '../notification.service';
@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  lookups:any={countries:[{id:0,name:'India'},{id:1,name:'Africa'},{id:2,name:'Europe'}]};
  data:any=[];
  constructor(private httpService:HttpserviceService,private notifyService:NotificationService) { }

  ngOnInit(): void {
    this.getData();
  }
  getData(){
    let _self=this;
    _self.httpService.getapi("getdata",{}).subscribe({
      next:data=>{
        if(data && data.result && data.result.length==0){
          _self.notifyService.showError("No Records Found!","Error")

        }else if(data && data.result && data.result.length>0){
          _self.data=data.result;
        }else{
          _self.notifyService.showError("Please Try Again","Error")
        }
      },error:err=>{
        _self.notifyService.showError("Invalid Details","Error")
      }
    })

  }
}
